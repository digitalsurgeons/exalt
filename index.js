#!/usr/bin/env node
const inquirer = require('inquirer')
const figlet = require('figlet')
const ora = require('ora')
// Make ora a global so I can access it anywhere
global.spinner = ora()
const dotenv = require('dotenv')
// Read .env file and add variables to process
dotenv.config()
const gitlab = require('node-gitlab-api')({
  url:   process.env.GL_URL,
  token: process.env.GL_ACCESS_TOKEN
});

// Internal Modules
const questions = require('./lib/questions')
const exaltFunctions = require('./lib/functions')

// Pass the answers to the implementation function to actually do everything
function implement(answers) {
  global.spinner.start(' ')
  exaltFunctions.setupEverything(answers.title, answers.slug, answers.description)
}

// Async display the title
function displayTitle(callback, font) {
  figlet('exalt', {font: font}, function(err, data) {
    if (err) {
        console.log('Something went wrong...');
        console.dir(err);
        return;
    }
    console.log('\n'+data)
    console.log('A surgery preparation utility\n')
    global.spinner.stop()
    callback(questions).then(function (answers) {
      implement(answers)
    })
  });
}

// Get all the available fonts so that we can pick a random one
fonts = figlet.fontsSync();

// Display the title with a random font and then call the prompt
displayTitle(inquirer.prompt, fonts[Math.floor(Math.random()*fonts.length)])
