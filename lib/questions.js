const slugify = require('slugify')
const exaltFunctions = require('./functions')

module.exports = [
  {
    type: 'input',
    name: 'name',
    message: 'What should this project be called?',
    validate: function (title) {
      return exaltFunctions.validateTitle(title)
    }
  },
  {
    type: 'input',
    name: 'slug',
    message: 'What is this project\'s slug? (This input will be slugified)',
    default: function(answers) {
      return slugify(answers.name).toLowerCase()
    },
    validate: function(slug) {
      var done = this.async();
      exaltFunctions.validateSlug(slug, done)
    },
    filter: function(input) {
      return slugify(input)
    }
  },
  {
    type: 'input',
    name: 'description',
    message: 'Briefly describe the project:'
  }
];
