#!/usr/bin/env node
var dotenv = require('dotenv')
dotenv.config()
var gitlab = require('node-gitlab-api')({
  url:   process.env.GL_URL,
  token: process.env.GL_ACCESS_TOKEN
});

async function getGroups() {
  let groups = await gitlab.groups.all()
  return groups
}

getGroups().then(function(result) {
  result.forEach(function(group) {
    console.log('--')
    console.log('Group ID: '+group.id)
    console.log('Group Name: '+group.name)
    console.log('Group Path: '+group.path)
    console.log('Web URL: '+group.web_url)
    console.log('--\n')
  })
})
