// Gitlab connection stuff
const gitlab = require('node-gitlab-api')({
  url:   process.env.GL_URL,
  token: process.env.GL_ACCESS_TOKEN
});
const node_ssh = require('node-ssh')
ssh = new node_ssh()

const simplegit = require('simple-git')()

// This part generates a new keypair when the application is launched
const keygen = require('keypair')
var forge = require('node-forge')
const keypair = keygen()
const publicKey = forge.pki.publicKeyFromPem(keypair.public);

// Get this list of ignored runners
const ignoredRunners = process.env.IGNORED_RUNNERS.split(',')

// Some ints I'm gonna use later
let completed_tasks = 0
let everything_tasks = 0
let num_runners = 0
let total_tasks = 6

// Connect to the SSH Server
ssh.connect({
  host: process.env.DEV_SERVER_IP,
  username: process.env.DEV_SERVER_USERNAME,
  privateKey: process.env.DEV_SERVER_PRIKEY,
})

// Check if everything has been setup (with something like bitwise operators), and then spit out the info and quit
var setupCheck = function(value) {
  everything_tasks = everything_tasks + value
  if (num_runners > 0) {
    if (everything_tasks == (total_tasks + num_runners)) {
      setTimeout(function() {
        global.spinner.info(`Web URL: ${global.project.web_url}`)
        global.spinner.succeed('Everything is done! Go forth and build, my mensch.')
        process.exit()
      }, 3000)
    }
  }
}

// Check if the first part has been completed (also with something like bitwise)
var checkfinish = function(done, value, message) {
  completed_tasks = completed_tasks + value
  if ((completed_tasks == 3) || (completed_tasks == 4)) {
    global.spinner.fail(message)
    done(null, false)
  }
  if (completed_tasks == 2) {
    global.spinner.stop()
    done(null, true)
  }
}

// Get a list of projects for the active user from gitlab that match the slug as a search string
async function checkGitLab(slug) {
  let projects = await gitlab.groups.listProjects(process.env.GL_NAMESPACE, {
    simple: true,
    search: slug
  })
  return projects
}

// Check to see if the folder exists on the dev server
async function checkServer(slug) {
  let response = await ssh.execCommand('ls -a', { cwd:'/var/www' })
  return response
}

// Setup the project using gitlab's API
async function registerProject(title, slug, description) {
  global.spinner.info('Setting up repo on gitlab')
  global.spinner.start()
  let result = await gitlab.projects.create({
    name: title,
    path: slug,
    description: description,
    namespace_id: process.env.GL_NAMESPACE,
    shared_runners_enabled: false,
    visibility: 'private',
  })
  return result
}

// Get the list of owned runners from gitlab
async function getRunnerList() {
  global.spinner.info('Getting runner list')
  global.spinner.start()
  let response = await gitlab.runners.owned()
  return response
}

// Add the specific key as a variable
async function setupRepoKey(id, key, value) {
  global.spinner.info('Setting up repo key '+key)
  global.spinner.start()
  let response = await gitlab.projects.variables.create(id, {
    key: key,
    value: value
  })
}

// Setup the runner with the specific project and runner IDs
async function setupRunner(projectId, runnerId) {
  global.spinner.info('Setting up runner '+runnerId)
  global.spinner.start()
  let response = await gitlab.projects.runners.enable(projectId, runnerId)
  return response
}

// Setup the directory on the dev server
async function setupDir(slug) {
  global.spinner.info('Setting up dev directory')
  global.spinner.start()
  let response = await ssh.execCommand('mkdir '+slug, { cwd:'/var/www' })
  return response
}

// Put the SSH key into authorized_keys so gitlab can ssh
async function addDevKey(slug) {
  var sshpubkey = forge.ssh.publicKeyToOpenSSH(publicKey, slug+'@gitlab-ci')
  global.spinner.info('Adding new key to authorized_keys')
  global.spinner.start()
  let response = await ssh.execCommand('echo "'+sshpubkey+'" >> ~/.ssh/authorized_keys', {})
  return response
}

async function cloneRepoLocally(projectSshUrl) {
  global.spinner.info('Cloning repository to the current directory')
  global.spinner.start()
  let cloneCompletePromise = new Promise((resolve, reject) => {
    simplegit.clone(projectSshUrl, function(error, data) {
      if (error) {
        reject(error)
      } else {
        resolve(data)
      }
    })
  })
  return cloneCompletePromise
}

// TODO: Commit a CI file for the project as the first commit so it starts doing empty deploys
// async function commitCIFile() {
//
// }

// Setup a generic function to use for the module
var exaltFunctions = function(callback) {}

// Validate whether the title is a defined piece of text or not
exaltFunctions.prototype.validateTitle = function(title) {
  if (typeof title == 'undefined' || title == "") {
    return false
  } else {
    return true
  }
}

// Checks both gitlab and the dev server to see if the project slug is taken
exaltFunctions.prototype.validateSlug = function(slug, done) {
  completed_tasks = 0
  global.spinner.text = "Checking availability on Gitlab and the Server"
  global.spinner.start()
  checkGitLab(slug).then(function(value) {
    value = value.filter(function(project) {
      return project.path.toLowerCase() == slug
    })
    if (value.length > 0) {
      checkfinish(done, 3, `The slug already exists on gitlab (${slug})`)
    } else {
      checkfinish(done, 1)
    }
  })
  checkServer(slug).then(function(value) {
    value = value.stdout.split("\n")
    value = value.filter(function(item) {
      return item.toLowerCase() == slug
    })
    if (value.length > 0) {
      checkfinish(done, 3, `The slug already exists on dsdev (${slug})`)
    } else {
      checkfinish(done, 1)
    }
  })
}

// The master function that sets up everything
exaltFunctions.prototype.setupEverything = function(title, slug, description) {
  registerProject(title, slug, description)
  .then(function(project) {
    global.project = project
    setupCheck(1)
    global.spinner.succeed('Repo created on gitlab')
    global.spinner.start()
    var sshpubkey = forge.ssh.publicKeyToOpenSSH(publicKey, slug+'@gitlab-ci')
    setupRepoKey(project.id, "SSH_PUBLIC_KEY", sshpubkey)
    .then(function(response) {
      setupCheck(1)
      global.spinner.succeed('SSH Public Key added to repo')
      global.spinner.start()
    })
    setupRepoKey(project.id, "SSH_PRIVATE_KEY", keypair.private.replace("\r", ""))
    .then(function(response) {
      setupCheck(1)
      global.spinner.succeed('SSH Private Key added to repo')
      global.spinner.start()
    })
    getRunnerList()
    .then(function(runners) {
      setupCheck(1)
      num_runners = runners.length
      runners.forEach(function(runner) {
        if (!ignoredRunners.includes(runner.id)) {
          setupCheck(1)
          setupRunner(project.id, runner.id)
          .then(function(result) {
            global.spinner.succeed(`Runner: ${runner.description} enabled`)
            global.spinner.start()
          })
        }
      })
    })
    cloneRepoLocally(project.ssh_url_to_repo).then(function (data) {
      global.spinner.succeed(`Repository cloned`)
      global.spinner.start()
    }).catch(function (data) {
      console.log(data)
      global.spinner.fail(`Couldn't clone the repository`)
      global.spinner.start()
    })
  })
  setupDir(slug).then(function(value) {
    setupCheck(1)
    global.spinner.succeed(`Directory created on dev server`)
    global.spinner.start()
  })
  addDevKey(slug).then(function(value) {
    setupCheck(1)
    global.spinner.succeed(`SSH key added to dev server`)
    global.spinner.start()
  })
}

module.exports = new exaltFunctions();
