# Exalt
## A surgery preparation tool

This tool:

* Checks for namespace availability
* Makes a gitlab repo
* Enables runners (optional)
* Adds a new directory on your dev server (optional) [^1]
* Sets up your gitlab.yml to deploy to your development server (optional) [^1]

[^1]: Requires your server to be SSH accessible and follow a standard /var/www convention

### Instructions

Get a personal access token from gitlab at:
https://gitlab.com/profile/personal_access_tokens

Copy .env.example to .env

Replace everything in .env with your relevant details. DEV_SERVER_PRIKEY needs to be a condensed single line of your private key, complete with \n new lines. You will need to place this file in the current directory of
wherever you intend to run the `exalt` or `exalt-groups` commands from. If you
keep all of your web projects in `~/Sites/`, then you will want it located at
`~/Sites/.env`.

Run the script and follow the instructions. Everything is interactive and
self-explanatory.

You can now clone your project using the URL provided by the script (or login
to Gitlab, and your new project will be at the top of the list)

#### Namespace ID

The namespace id is numeric, and cannot be retrieved from the
Gitlab UI. To retrieve it, you can run `exalt-groups`, which will list all
the groups you have access to. The `Group ID` value is the one you want.

#### SSH Private Key

For the private key to your server you'll need to generate a passwordless key
if you don't already have one. You can do this by running `ssh-keygen -t rsa -b
4096 -f id_exalt -C '<something-to-identify-this-key>'`. Once created you can
copy the public key to the development server and copy the private key into
your `.env` file. Note that the format of the `.env` file prohibits newlines,
so you will need to transform the private key into a single line with literal
`\n` where real newlines used to exist, and double-quote the entire value. If
you are using an editor with regex find-and-replace support, you can select the
lines of the private key and do the following replacement `s/\n/\\n/`. 

Example:

```shell
DEV_SERVER_PRIKEY="-----BEGIN RSA PRIVATE KEY-----\nProc-Type: 4,ENCRYPTED\nDEK-Info: AES-128-CBC,abcdefgh12345676\n\nabcdefgh1234567890\nabcdefgh1234567890\n-----END RSA PRIVATE KEY-----\n"
```

Once you've copied it into your `.env` file, you can delete the temporary
`id_exalt` files. Be sure you keep your `.env` file safe, and always consider
having a deployment-only user for your development server.

### Next Steps (DS Specific)
1. Clone either of the Digital Surgeons Boilerplate repos:
   1. https://github.com/digitalsurgeons/boilerplate-wordpress
   2. https://github.com/digitalsurgeons/boilerplate-craftcms
2. Copy the contents of these repos into your fresh project folder. (Remember to skip the `.git`  folder!)
3. Add a new DB to your local setup
   1. Import the SQL file in the repo
4. Add a new DB to DSDev
   1. Import the SQL file in the repo
5. Modify the config based on your chosen CMS for the DB. (Make sure you config both the dev server and your local environment)
   1. The wordpress config is located at: boilerplate-wordpress/public_html/wp-config.env.php
   2. The Craft CMS config is located at: boilerplate-craftcms/craft/config/db.php
6. Modify the `.gitlab-ci.yml` :
   1. Look for the line that contains `deployments@162.243.173.73:/var/www/ds-boilerplate/`
   2. Replace `ds-boilerplate` with the chosen slug of your project
7. Run `npm install`
8. Run `site=[site url here] npm run dev`
9. `cd` into your new project folder and run `git flow init` to ensure git flow is enabled.
10. Push your first commit, and you are all set
